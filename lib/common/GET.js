export async function GET(url) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = (() => {
            console.log(xhr.status, xhr.readyState);
            if (xhr.status === 200 && xhr.readyState === 4) {
                resolve(xhr.responseText);
            }
        });
        xhr.send();
    })
}