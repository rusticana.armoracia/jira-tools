export async function POST(url, params) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);

        xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xhr.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");

        xhr.onreadystatechange = (() => {
            console.log(xhr.status, xhr.readyState);
            if (xhr.status === 200 && xhr.readyState === 4) {
                resolve(xhr.responseText);
            }
        });
        xhr.send(JSON.stringify(params));
    })
}