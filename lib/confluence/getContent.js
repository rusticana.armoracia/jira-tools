import {GET} from '../common/GET.js';

import {chromeExtension} from '../chrome-extensions.js';

async function getRawText (pageId) {
    return GET(`https://confluence.otpbank.ru/rest/api/content/${pageId}?expand=body.storage`);
}

function parseContent (rawText) {
    let text = JSON.parse(rawText).body.storage.value;

    function unique (accum, item) {
        if (accum.indexOf(item) === -1) {
            accum.push(item);
        }
        return accum;
    }

    let nsEntries = [...text.matchAll(/<[a-z]+:/gi)]
        .map(item => item[0].replace('<', '').replace(':', ''))
        .reduce(unique, []);

    //not used anymore
    let entities = [...text.matchAll(/&[a-z]+;/gi)]
        .map(item => item[0].replace('&', '').replace(';', ''))
        .reduce(unique, []);


    let wrappedText = `<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE html>
    <html ${nsEntries.map((item, index) => `xmlns:${item}="uri${index}"`).join(' ')}>
        ${text}
    </html>
    `;

    return new DOMParser().parseFromString(wrappedText, 'text/html');
}

export async function getContent (tabId, pageId) {
    let fn = async function (acknowledgeExtension, pageId) {
        let content = await getRawText(pageId);
        console.log(content);
        acknowledgeExtension(content);
    };


    let text = await chromeExtension.execute.asFunction(tabId, fn, [getRawText, GET], pageId);

    return parseContent(text);
}