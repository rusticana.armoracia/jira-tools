import {GET} from '../common/GET.js';
import { chromeExtension } from '../chrome-extensions.js';

async function getChangesInternal (jql) {
    let startAt = 0;
    let maxResults = 200;
    let total = 0;
    let done = false;
    let issues = [];


    let spawnRequest = () => GET(`https://jira.otpbank.ru/rest/api/2/search?jql=${jql}&expand=changelog&fields=summary&startAt=${startAt}&maxResults=${maxResults}`);
    let iterator = {
        [Symbol.asyncIterator]() {
            return {
                async next() {
                    return {
                        value: spawnRequest(),
                        done
                    }
                }
            }
        }
    };

    for await (let stream of iterator) {
        let chunk = JSON.parse(await stream);
        total = chunk.total;
        if (chunk.startAt + maxResults >= total) {
            done = true;
        }
        startAt = maxResults;
        issues = [...issues, ...chunk.issues];
    }
    console.log(issues);

    return issues
        .map(issue => {
            return issue.changelog.histories.map(history => {
                return history.items.map(item => {
                    return {
                        issueId: issue.id,
                        key: issue.key,
                        summary: issue.fields.summary,
                        changeId: history.id,
                        author: history.author.key,
                        created: history.created,
                        timestamp: new Date(history.created).getTime(),
                        field: item.field,
                        from: item.from,
                        to: item.to
                    }
                }).flat()
            }).flat()
        }).flat()
}

export async function getChanges(tabId, jql) {
    return await chromeExtension.execute.asFunction(tabId, async (acknowledgeExtension, jql) => {
        acknowledgeExtension(await getChangesInternal(jql));
    }, [GET, getChangesInternal], jql);
}