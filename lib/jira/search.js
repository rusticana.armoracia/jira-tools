import { GET } from "../common/GET.js";
import { chromeExtension } from "../chrome-extensions.js";

async function searchInternal (jql, fields = ['summary']) {
    return JSON.parse(await GET(`https://jira.otpbank.ru/rest/api/2/search?jql=${jql}&fields=${fields.join(',')}`)).issues;
}

export async function search (tabId, jql, fields) {
    return await chromeExtension.execute.asFunction(tabId, async (acknowledgeExtension, jql, fields) => {
        acknowledgeExtension(await searchInternal(jql, fields));
    }, [GET, searchInternal], jql, fields);
}