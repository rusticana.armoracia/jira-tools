import {GET} from '../../common/GET.js';
import {POST} from '../../common/POST.js';
import { chromeExtension } from '../../chrome-extensions.js';

async function getItemsInternal(
    structureId,
    filterFn = () => true, 
    attributes = [{id: "key", format: "text"}, {id: "summary", format: "text"}]
) {
    let {formula} = JSON.parse(await GET(`https://jira.otpbank.ru/rest/structure/2.0/forest/latest?s={%22structureId%22:${structureId}}`));
    
    let spec = formula
        .split(',')
        .map(item => {
            let chunks = item.split(':');
            let rowId = chunks[0];
            let level = chunks[1];
            let type, id;
            if (chunks[2].indexOf('/') === -1) {
                type = 'issue';
                id = chunks[2];
            }
            else {
                type = chunks[2].split('/')[0];
                id = chunks[2].split('/')[1];
            }

            return {level, rowId, type, id};
        });

    let rows = spec
        .filter(filterFn)
        .map(item => item.rowId);

    console.log(rows);

    let data = JSON.parse(await POST('https://jira.otpbank.ru/rest/structure/2.0/value', {
        requests: [{
            forestSpec: {
                structureId
            },
            rows,
            attributes
        }]
    })).responses[0].data;

    return rows.map((rowId, index) => {
        return data.reduce((accum, {attribute: {id}, values}) => {
            accum[id] = values[index];

            return accum;
        }, {rowId});
    });
}

export async function getItems (
    tabId,
    structureId,
    filterFn = () => true, 
    attributes = [{id: "key", format: "text"}, {id: "summary", format: "text"}]) {
        return await chromeExtension.execute.asFunction(tabId, async (acknowledgeExtension, structureId, filterFn, attributes) => {
            acknowledgeExtension(await getItemsInternal(structureId, filterFn, attributes));
        }, [GET, POST, getItemsInternal], structureId, filterFn, attributes);
    }