export async function simpleExecuteFile (tabId, file) {
    return new Promise((resolve, reject) => {
        try {
            chrome.tabs.executeScript(tabId, {
                file: chrome.runtime.getURL(file)
            }, resolve);
        }
        catch(e) {
            reject(e);
        }
    })
}
