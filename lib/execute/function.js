function getText(provide, body) {
    return (provide.map(item => item.toString()).join(' ') + ' ' + body)
        .replaceAll('`', '\\`').replaceAll('$', '\\$');
}

function wrap(fn, provide, ...args) {
    let body = '(' + fn.toString() + ').apply(this, [' + args
        .map((item, index) => 
            typeof item === 'object' ? JSON.stringify(item) : 
            //typeof item === 'string' && index > 0 ? `\`${item}\`` : 
            item)
        .join(', ') +']);';

    let text = getText(provide, body);

    return `(() => {
        let script = document.createElement('script');
        script.setAttribute('type', 'module');
        script.text = \`` + text + `\`;
        document.body.appendChild(script);
    })()`
}

/**
 * @param {number} tabId - id of chrome.tab to inject script
 * @param {Function} fn - function to execute in a tab
 * @param {Function[]} provide - dependencies 
 * 
 * NOT working as intended!
 */
export async function asFunction(tabId, fn, provide = [], ...args) {
    return new Promise (resolve => {
        let random = Math.random().toString();
        let extensionId = chrome.runtime.getURL('')
            .replace('chrome-extension://', '')
            .replace('/', '');
        let listener = (message) => {
            let {random: randomSeed, payload} = message;
            console.log(randomSeed, random);
            if (randomSeed == random) {
                chrome.runtime.onMessageExternal.removeListener(listener);
                resolve(payload);
            }
        };
        chrome.runtime.onMessageExternal.addListener(listener);

        let acknowledgeExtension = ((payload) => chrome.runtime.sendMessage('`${extensionId}`', {random: `${random}`,payload: payload}))
            .toString()
            .replace('`${random}`', random)
            .replace('`${extensionId}`', extensionId);
            
        let code = wrap(fn, provide, acknowledgeExtension, ...args);

        //console.log(code);

        chrome.tabs.executeScript(tabId, {code});
    });
}
