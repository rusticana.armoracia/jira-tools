export async function executeFile(tabId, file) {
    console.log(tabId, file);
    return new Promise(resolve => {
        let url = chrome.runtime.getURL(file);
        //let extensionId = chrome.runtime.getURL('').replace('chrome-extension://', '').replace('/', '');
        let random = Math.random().toString();
        let listener = (message) => {
            let {random: randomSeed, payload} = message;
            if (randomSeed === random) {
                chrome.runtime.onMessage.removeListener(listener);
                resolve(payload);
            }
        };
        chrome.runtime.onMessage.addListener(listener);

        chrome.tabs.executeScript(tabId, {code: `
            (async () => {
                let script = document.createElement('script');
                script.type = 'module';
                
                let code = await new Promise(resolve => {
                    let xhr = new XMLHttpRequest();
                    xhr.open('GET', '${url}', true);
                    xhr.onreadystatechange = (event => {
                        if (xhr.status === 200 && xhr.readyState === 4) {
                            resolve(xhr.responseText);
                        }
                    });
                    xhr.send();
                });
                
                code = \`
                (() => {
                    function acknowledgeExtension(payload) {
                        window.postMessage({
                            random: '${random}',
                            payload: payload
                        })
                    }
                    try {
                \` + code + \`
                    }
                    catch(e) {
                        console.error(e)
                    }
                    finally {
                        console.log('${file} injected!');
                    }
                })();
                \`;
                
                script.textContent = code;
                
                let listener = ({data}) => {
                    let {payload, random: randomSeed} = data;
                    if (randomSeed === '${random}') {
                        window.removeEventListener('message', listener);
                        chrome.runtime.sendMessage(/*${tabId}, */data);
                    }
                    
                };
                window.addEventListener('message', listener);
                
                document.body.appendChild(script);
            })();
        `});
    });

}