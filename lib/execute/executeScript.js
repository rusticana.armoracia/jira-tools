import {simpleExecuteFile} from './simple.js';
import {executeFile} from './file.js'; 
import {asFunction} from './function.js';

export const execute = {
    inject: simpleExecuteFile,
    file: executeFile,
    asFunction,
};
