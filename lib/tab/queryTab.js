export async function queryTab(url) {
    return new Promise(resolve => {
        chrome.tabs.query({ url }, resolve);
    });
}
