export async function focusTab(tab) {
    await new Promise(resolve => chrome.windows.update(tab.windowId, { focused: true }, resolve));
    return new Promise(resolve => chrome.tabs.update(tab.id, { active: true }, resolve));
}
