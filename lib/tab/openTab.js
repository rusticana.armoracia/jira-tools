export async function openTab(url) {
    return new Promise(resolve => {
        chrome.tabs.create({ active: false, url }, resolve);
    });
}
