export async function getById(tabId) {
    return new Promise(resolve => chrome.tabs.query({ id: tabId }, tab => resolve(tab[0])));
}
