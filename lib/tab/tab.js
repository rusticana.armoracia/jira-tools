import {getActiveTab} from './getActiveTab.js';
import {openTab} from './openTab.js';
import {removeTab} from './removeTab.js';
import {queryTab} from './queryTab.js';
import {openSingletonTab} from './openSingletonTab.js';
import {focusTab} from './focusTab.js';
import {getById} from './getById.js';

export const tab = {
    focus: focusTab,
    getActive: getActiveTab,
    openSingle: openSingletonTab, 
    open: openTab, 
    query: queryTab,
    remove: removeTab, 
    getById: getById,
};