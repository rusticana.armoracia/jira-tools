export async function getActiveTab() {
    return new Promise(resolve => chrome.tabs.query({ active: true }, tab => resolve(tab[0])));
}
