export async function removeTab(tabId) {
    return new Promise(resolve => {
        chrome.tabs.remove(tabId, resolve);
    });
}
