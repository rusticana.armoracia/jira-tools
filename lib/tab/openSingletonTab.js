import { queryTab } from './queryTab.js';
import { openTab } from './openTab.js';

export async function openSingletonTab(url) {
    let existingTab = await queryTab(url)[0];
    return existingTab ? existingTab[0] : await openTab(url);
}
