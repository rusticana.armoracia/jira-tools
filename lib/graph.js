export class Graph {
    constructor (canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.data = {};
        this.hasData = false;

        this.config = {
            padding: 10,
            width: this.canvas.width * 1,
            height: this.canvas.height * 1,
            color: {
                stroke: 'black',
                fill: 'white',
                text: 'black'
            }
        };
    }

    loadData (maxX, minX, minY, maxY, steps) {
        this.data = {maxX, minX, minY, maxY, steps};
        this.hasData = true;
    }

    loadDataSmart (steps) {
        this.data.steps = steps.sort((a, b) => a.timestamp - b.timestamp);
        let x = this.data.steps.map(item => item.timestamp);
        this.data.minX = Math.min(...x);
        this.data.maxX = Math.max(...x);
        this.data.minY = 0;//todo - fixme!
        this.data.maxY = this.data.steps.reduce((accum, item) => accum + item.delta, 0);
    }

    clear () {
        this.ctx.clear();
    }

    setSize (width, height) {
        this.canvas.width = width;
        this.canvas.height = height;
        this.config.width = width;
        this.config.height = height;
    }

    drawAxis () {
        let {padding, width, height} = this.config;
        this.ctx.beginPath();
        this.ctx.moveTo(padding, padding);
        this.ctx.lineTo(padding, height - padding);
        this.ctx.lineTo(width - padding, height - padding);
        this.ctx.strokeStyle = this.config.color.stroke;
        this.ctx.stroke();
        this.ctx.closePath();
    }

    drawLadder () {
        if (!this.hasData) {
            throw new Error('load data first!');
        }

        let {ctx, config: {padding, height}} = this;

        ctx.beginPath();
        ctx.moveTo(padding, height - padding);
        let prevX, prevY, currentX, currentY, value;
        for ({prevX, prevY, currentX, currentY, value} of spawnIterator(this)) {
            ctx.lineTo(currentX, prevY);
            ctx.lineTo(currentX, currentY);
        }
        this.ctx.strokeStyle = this.config.color.stroke;
        ctx.stroke();
        this.ctx.fillStyle = this.config.color.text;
        this.ctx.fillText(value, currentX, currentY);
        ctx.lineTo(currentX, height - padding);
        this.ctx.closePath();
        this.ctx.fillStyle = this.config.color.fill;
        this.ctx.fill();
    }

    addVerticalRuler (x, text) {
        if (!this.hasData) {
            throw new Error('load data first!');
        }
        this.data.steps = [...this.data.steps, {timestamp: x, delta: 0, text}]
            .sort((a, b) => a.timestamp - b.timestamp);
        this.data.minX = Math.min(this.data.minX, x);
        this.data.maxX = Math.max(this.data.maxX, x);
    }

    drawVerticalRulers () {
        let {
            ctx,
            data: {steps, minX, maxX},
            config: {width, height, padding}
        } = this;
        let totalXUnits = maxX - minX;
        let availableWidth = width - 2 * padding;
        let xCanvasPerUnit = totalXUnits / availableWidth;

        steps.filter(item => item.text)
            .forEach(({timestamp, text}) => {
                let x = padding + (timestamp - minX) / xCanvasPerUnit;
                ctx.beginPath();
                ctx.moveTo(x, padding);
                ctx.lineTo(x, height - padding);
                ctx.stroke();
                this.ctx.strokeStyle = this.config.color.stroke;
                ctx.closePath();
                ctx.save();
                ctx.translate(x, height - padding);
                ctx.rotate(-Math.PI / 2);
                this.ctx.fillStyle = this.config.color.text;
                ctx.fillText(text, 0, 0);
                ctx.rotate(Math.PI / 2);
                ctx.translate(-x, -height + padding);
                ctx.restore();
            })
    }

    applyBounds (graph) {
        let {data: {maxX, minX, minY, maxY}} = graph;

        this.data = {steps: this.data.steps, maxX, minX, minY, maxY};
    }

    setColor (stroke, fill, text) {
        this.config.color = {stroke, fill, text};
    }
}

export function processEvents (events) {
    let minX = Math.min(...events.map(item => item.timestamp));
    let maxX = Math.max(...events.map(item => item.timestamp));
    let minY = 0;

    let steps = events.map(item => {
        let delta;
        switch (item.type) {
            case 'epic created':
                delta = 1;
                break;
            case 'issue added to epic':
                delta = 1;
                break;
            case 'issue removed from epic':
                delta = -1;
                break;
            case 'issue done':
                delta = 1;
                break;
            case 'issue undone':
                delta = -1;
                break;
            default:
                throw new Error('unexpected event type ' + event.type);
        }

        return {...item, delta}
    });

    let maxY = steps
        .map(item => item.delta)
        .reduce((accum, value) => accum + value, 0);

    return {maxX, minX, minY, maxY, steps};
}

function spawnIterator (graph) {
    let index = 0;
    let maxIndex = graph.data.steps.length - 1;
    let currentX = graph.config.padding;
    let currentY = graph.config.height - graph.config.padding;
    let prevX = currentX;
    let prevY = currentY;
    let totalXUnits = graph.data.maxX - graph.data.minX;
    let totalYUnits = graph.data.maxY - graph.data.minY;
    let availableWidth = graph.config.width - 2 * graph.config.padding;
    let availableHeight = graph.config.height - 2 * graph.config.padding;
    let xCanvasPerUnit = totalXUnits / availableWidth;
    let yCanvasPerUnit = totalYUnits / availableHeight;
    let currentStep = {
        x: graph.data.minX,
        y: graph.data.minY
    };

    return {
        [Symbol.iterator]: () => {
            return {
                next: () => {
                    let step = graph.data.steps[index];
                    currentStep.x = step.timestamp;
                    currentStep.y += step.delta;

                    prevX = currentX;
                    prevY = currentY;
                    currentX = graph.config.padding + (currentStep.x - graph.data.minX) / xCanvasPerUnit;
                    currentY = graph.config.height - graph.config.padding - (currentStep.y - graph.data.minY) / yCanvasPerUnit;

                    let done = index >= maxIndex;
                    let value = {prevX, prevY, currentX, currentY, value: currentStep.y};
                    if (step.text) {
                        value.text = step.text;
                    }

                    let result = {done, value};

                    index++;

                    return result;
                }
            };
        }
    };
}