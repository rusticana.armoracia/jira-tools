import {executeScript, chromeExtension} from './lib/chrome-extensions.js';

let button = document.querySelector('button#copy');

button.addEventListener('click', async () => {
    let tab = await chromeExtension.tab.open('https://jira.otpbank.ru/');

    let data = await executeScript(tab.id, 'getData.js');

    await navigator.clipboard.writeText(JSON.stringify(data));

    chromeExtension.tab.remove(tab.id);

    window.close();
});

