import {chromeExtension} from './lib/chrome-extensions.js';

console.log('I am background page!');

chrome.commands.onCommand.addListener(async function(command) {
  console.log('Command:', command);
  switch (command) {
    case 'show-plugin-page': {
      let tab = await chromeExtension.tab.open(chrome.runtime.getURL('plugin-page.html'));
      await chromeExtension.tab.focus(tab);
      break;
    }
    default: break;
  }
});
