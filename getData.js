function GET (url, params) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = (event => {
            if (xhr.status === 200 && xhr.readyState === 4) {
                resolve(xhr.responseText);
            }
        });
        xhr.send();
    });
}

(async () => {
    let {formula} = JSON.parse(await GET('https://jira.otpbank.ru/rest/structure/2.0/forest/latest?s={%22structureId%22:4004}'));

    let parsedFormula = formula
        .split(',')
        .filter(item => item.indexOf('/') === -1)
        .map(item => item.split(':'));

    let jql = `id in (${parsedFormula.map(([rowId, level, issueId]) => issueId).join(', ')})`;
    let fields = ['issuetype', 'summary', 'status', 'project'].join(', ');
    let searchResult = JSON.parse(await GET(`https://jira.otpbank.ru/rest/api/2/search?jql=${jql}&fields=${fields}`));

    let data = searchResult.issues.map(issue => {
        return {
            id: issue.id,
            key: issue.key,
            summary: issue.fields.summary,
            type: issue.fields.issuetype.name,
            status: issue.fields.status.name,
            project: issue.fields.project.name
        };
    });

    //console.log(searchResult.issues)

    acknowledgeExtension(data);
})();

