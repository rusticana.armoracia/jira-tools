function GET (url) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = (() => {
            if (xhr.status === 200 && xhr.readyState === 4) {
                resolve(xhr.responseText);
            }
        });
        xhr.send();
    });
}

(async () => {
    let {formula} = JSON.parse(await GET('https://jira.otpbank.ru/rest/structure/2.0/forest/latest?s={%22structureId%22:4004}'));

    let parsedFormula = formula
        .split(',')
        .filter(item => item.indexOf('/') === -1)
        .map(item => item.split(':'));

    let jql = `id in (${parsedFormula.map(([rowId, level, issueId]) => issueId).join(', ')})`;
    let fields = ['issuetype', 'summary', 'status', 'project'].join(', ');
    let searchResult = JSON.parse(await GET(`https://jira.otpbank.ru/rest/api/2/search?jql=${jql}&fields=${fields}&expand=changelog`));

    let issues = searchResult.issues.map(issue => {
        return {
            id: issue.id,
            key: issue.key,
            summary: issue.fields.summary,
            type: issue.fields.issuetype.name,
            status: issue.fields.status.name,
            project: issue.fields.project.name
        };
    });
    
    let issueMap = issues.reduce((accum, issue) => {
        accum[issue.id] = issue;
        return accum;
    }, {});
    
    let changes = searchResult.issues.reduce((accum, entry) => {
        let issue = issueMap[entry.id];
        console.log(entry, issue);
        accum = [...accum, ...entry.changelog.histories.map(history => {
            let timestamp = new Date(history.created).getTime();
            return history.items.map(item => {
                return {issue, timestamp, ...item};
            });
        }).flat()];
        return accum;
    }, []);

    acknowledgeExtension({issues, changes});
})();
