import {chromeExtension} from './lib/chrome-extensions.js';
import {getContent} from './lib/confluence/getContent.js';
import {GET} from './lib/common/GET.js';
import {POST} from './lib/common/POST.js';
import { getChanges } from './lib/jira/getChanges.js';
import { Graph, processEvents } from './lib/graph.js';
import { search } from './lib/jira/search.js';

let button = document.querySelector('button#copy');

button.addEventListener('click', async () => {
    let tab = await chromeExtension.tab.open('https://jira.otpbank.ru/');

    let {
        structureId,
        status: STATUS,
        initialRequest: {jql, fields},
        rulers,
    } = JSON.parse(await GET('options.json'));

    rulers.push({timestamp: Date.now(), text: 'now'});

    let items = (await search(tab.id, jql, fields))
        .map(item => { return {...item, timestamp: new Date(item.fields.created).getTime()}});

    console.log(items);
    document.querySelector('input#epics-count').value = items.length;

    let changes = await getChanges(tab.id, `'issue in (${items.map(item => item.key).join(', ')})'`);
    document.querySelector('input#changes-count').value = changes.length;
    document.querySelector('input#epic-child-change-count').value = changes
        .filter(item => item.field === 'Epic Child').length;

    document.querySelector('input#component-change-count').value = changes
        .filter(item => item.field === 'Component').length;

    let events = [
        ...changes
            .filter(item => ['status', 'Epic Child'].indexOf(item.field) !== -1)
            .map(item => {
                let type;
                if (item.field === 'status') {
                    if (!STATUS[item.from]) {
                        console.log(item.from);
                    }
                    if (!STATUS[item.to]) {
                        console.log(item.to)
                    }
                    if (STATUS[item.to] !== 'done') {
                        type = 'skip'
                    }
                    else {
                        debugger;
                    }
                }
                else if (item.field === 'Epic Child') {
                    if (item.from === null) {
                        type = 'issue added to epic';
                    }
                    else if (item.to === null) {
                        type = 'issue removed from epic';
                    }
                }
                return {
                    timestamp: item.timestamp,
                    item,
                    type
                };
        })
        .filter(item => item.type !== 'skip')
    ].sort((a, b) => a.timestamp - b.timestamp);

    document.querySelector('input#first-change-date').value = new Date(events[0].timestamp).toLocaleDateString();
    document.querySelector('input#last-change-date').value = new Date(events[events.length - 1].timestamp).toLocaleDateString();

    console.log(events);
    let itemsChanges = await getChanges(tab.id, `'"Epic Link" in (${items.map(item => item.key).join(', ')})'`);
    let doneChanges = (itemsChanges)
        .filter(item => item.field === 'status' && STATUS[item.to] === 'done')
        .sort((a, b) => a.timestamp - b.timestamp);

    let undoneEvents =  itemsChanges
        .filter(item => item.field === 'status' && STATUS[item.from] === 'done')
        .map(item => {
            return {
                timestamp: item.timestamp,
                type: 'issue undone',
                item
            };
        });

    let doneEvents = processEvents(doneChanges.map(item => {return {
        timestamp: item.timestamp,
        type: 'issue done',
        item
    }}));
    let canvas = document.querySelector('canvas#graph');
    let graph = new Graph(canvas);
    graph.setSize(1000, 400);
    let doneGraph = new Graph(canvas);
    let axisGraph = new Graph(canvas);
    graph.setColor('#90caf9', '#bbdefb', '#42a5f5');
    doneGraph.setColor('#81c784', '#a5d6a7', '#66bb6a');
    axisGraph.drawAxis();
    let {maxX, minX, minY, maxY, steps} = processEvents(events);

    graph.loadData(maxX, minX, minY, maxY, steps);
    doneGraph.loadData(doneEvents.maxX, doneEvents.minX, doneEvents.minY, doneEvents.maxY, doneEvents.steps);
    rulers.forEach(({timestamp, text}) => graph.addVerticalRuler(timestamp, text));
    doneGraph.applyBounds(graph);
    graph.drawLadder();
    doneGraph.drawLadder();
    graph.drawVerticalRulers();

});



let confluenceButton = document.querySelector('button#confluence');

confluenceButton.addEventListener('click', async () => {
    let tab = await chromeExtension.tab.open('https://confluence.otpbank.ru/');

    let {confluence: {pageId}} = JSON.parse(await GET('options.json'));

    let content = await getContent(tab.id, pageId);

    console.log(content);

    await chromeExtension.tab.remove(tab.id);
});