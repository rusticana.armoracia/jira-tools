(async () => {
    async function GET(url) {
        return new Promise(resolve => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.onreadystatechange = (() => {
                if (xhr.status === 200 && xhr.readyState === 4) {
                    resolve(xhr.responseText);
                }
            });
            xhr.send();
        })
    }

    function toDuration(time) {
        let millis = time % 1000;
        time = Math.floor(time / 1000);
        let sec = time % 60;
        time = Math.floor(time / 60);
        let min = time % 60;
        time = Math.floor(time / 60);
        let hrs = time % 24;
        time = Math.floor(time / 24);
        let days = time % 7;
        time = Math.floor(time / 7);
        let weeks = time;

        let string = (weeks ? `${weeks}w ` : '') +
            (days ? `${days}d ` : '') +
            (hrs ? `${hrs}h ` : '') +
            (min ? `${min}m ` : '');

        return string.trim() || '0';
    }

    let pendingTask = null;

    let STATUS = JSON.parse(await GET('https://jira.otpbank.ru/rest/api/2/status')).
        reduce((accum, value) => {
            accum[value.id] = value.name;
            return accum;
        }, {});

    document.body.addEventListener('mouseover', e => {
        let a = e.path.filter(item => item instanceof HTMLAnchorElement)[0];
        let key;
        if (a) {
            let href = a.href;
            if (href.indexOf('https://jira.otpbank.ru/browse/') !== -1) {
                key = href.replace('https://jira.otpbank.ru/browse/', '').replace('/', '');
            }
            else {
                return;
            }
            if (pendingTask === null || pendingTask.key !== key) {
                pendingTask = {
                    key,
                    promise: new Promise(async resolve => {
                        let result = JSON.parse(await GET(`https://jira.otpbank.ru/rest/api/2/search?jql=issue=${key}&expand=changelog&fields=created,status`));
                        if (pendingTask && pendingTask.key === key) {
                            pendingTask = null;

                            let issue = result.issues[0];

                            let changes = issue.changelog.histories.map(history => {
                                return history.items.map(item => {
                                    return {
                                        timestamp: new Date(history.created).getTime(),
                                        field: item.field,
                                        from: item.from,
                                        to: item.to
                                    }
                                }).flat()
                            }).flat();

                            let fields = changes
                                .map(item => item.field)
                                .reduce((accum, item) => {
                                    if (accum.indexOf(item) === -1) {
                                        accum.push(item);
                                    }
                                    return accum;
                                }, []);
                            
                            //console.log(fields);
                            
                            let flagged = false;

                            let flaggedChanges = 
                            [
                                {
                                    timestamp: new Date(issue.fields.created).getTime(),
                                    field: 'Flagged',
                                    from: null,
                                    to: true
                                },
                                ...changes
                                    .filter(item => item.field === 'Flagged' || item.field === 'Флаг')
                                    .map(item => {
                                        flagged = !flagged;
                                        return {
                                            ...item,
                                            from: !flagged,
                                            to: flagged
                                        }
                                    }),
                                {
                                    timestamp: Date.now(),
                                    field: 'Flagged',
                                    from: flagged,
                                    to: flagged
                                }
                            ]

                            changes = [{
                                timestamp: new Date(issue.fields.created).getTime(),
                                field: 'status',
                                from: null,
                                to: null
                            }, 
                            ...changes, 
                            {
                                timestamp: Date.now(),
                                field: 'status',
                                from: issue.fields.status.id,
                                to: null
                            },
                            ]
                                .filter(item => item.field === 'status')
                                .sort((a, b) => a.timestamp - b.timestamp);

                            changes[0].to = changes[1].from;

                            let timeInStatus = changes.reduce((accum, item, index) => {
                                let nextItem = changes[index + 1];
                                if (!nextItem) {
                                    return accum;
                                }
                                let status = STATUS[nextItem.from];
                                let duration = toDuration(nextItem.timestamp - item.timestamp);

                                accum = [...accum, {status, duration, millis: nextItem.timestamp - item.timestamp}];
                                return accum;
                            }, []);
                            let flaggedTime = flaggedChanges.reduce((accum, item, index) => {
                                let nextItem = flaggedChanges[index + 1]
                                if (!nextItem) {
                                    return accum;
                                }
                                if (index % 2 === 1 && nextItem) {
                                    accum += nextItem.timestamp - item.timestamp;
                                }
                                return accum;
                            }, 0);
                            //console.log(flaggedChanges);
                            let totalTime = flaggedChanges[flaggedChanges.length - 1].timestamp - 
                                flaggedChanges[0].timestamp;
                            let flaggedInfo = {
                                totalTime: toDuration(totalTime),
                                flaggedTime: toDuration(flaggedTime),
                                ratio: (flaggedTime / totalTime).toFixed(2) * 100 + '%'
                            };

                            console.log(key, timeInStatus, flaggedInfo);
                        }
                    })
                }
            }
        }
    });
})();
